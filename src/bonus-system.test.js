import { calculateBonuses } from './bonus-system.js';

describe('Bonus system', () => {
  it('calculates the multiplier correctly', () => {
    expect(calculateBonuses('Standard', 1000)).toBe(0.05);
    expect(calculateBonuses('Premium', 1000)).toBe(0.1);
    expect(calculateBonuses('Diamond', 1000)).toBe(0.2);
  });
  
  it('calculates the bonus correctly', () => {
    expect(calculateBonuses('Standard', 1000)).toBeCloseTo(0.05, 2);
    expect(calculateBonuses('Premium', 10000)).toBeCloseTo(0.15, 2);
    expect(calculateBonuses('Diamond', 50000)).toBeCloseTo(0.4, 2);
    expect(calculateBonuses('Standard', 100000)).toBeCloseTo(0.125, 3);
  })
  
  it('fails with the wrong program name', () => {
    expect(calculateBonuses('test', 1000)).toBe(0);
  });
});
